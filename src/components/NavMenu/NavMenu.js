import React from 'react';
import {Nav, NavItem} from 'reactstrap';
import {NavLink} from "react-router-dom";

const NavMenu = (props) => {
    {
        return (
            <Nav>
                <NavItem>
                    <NavLink to="/" exact className="nav-link">Home</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to="/aboutus" exact className="nav-link">About Us</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to="/contacts" exact className="nav-link">Contacts</NavLink>
                </NavItem>
                <NavItem>
                    {props.children}
                </NavItem>
            </Nav>
        );
    }
};

export default NavMenu;