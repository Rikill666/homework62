import React from 'react';

const Magic = (props) => {
    return (
            <h2>
                {props.mag.name +" " + props.mag.surname}
            </h2>
    );
};

export default Magic;