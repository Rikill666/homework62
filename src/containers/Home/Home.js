import React, {Component, Fragment} from 'react';
import NavMenu from "../../components/NavMenu/NavMenu";
import {Container, NavLink, Row} from "reactstrap";
import CountriesList from "../../components/CountriesList/CountriesList";
import CountryInfo from "../../components/CountryInfo/CountryInfo";
import axios from "axios";
import "./Home.css";


class Home extends Component {
    state = {
        countries: [],
        selectedCountryCode: ""
    };
    goToCountryPage = () => {
        const queryParams = [];
        queryParams.push(encodeURIComponent(0) + '=' + encodeURIComponent(this.state.selectedCountryCode));
        const queryString = queryParams.join('&');
        this.props.history.push({pathname: '/checkout', search: '?' + queryString});
    };
    countryChoice = (code) => {
        this.setState({selectedCountryCode: code});
    };

    async componentDidMount() {
        const response = await axios.get("https://restcountries.eu/rest/v2/all?fields=name;alpha3Code");
        const countries = response.data;
        this.setState({countries});
    }

    render() {
        return (
            <Fragment>
                <NavMenu>
                    <NavLink href="" onClick={this.goToCountryPage}>Link</NavLink>
                </NavMenu>
                <Container style={{marginTop: "10px"}}>
                    <Row>
                        <CountriesList countries={this.state.countries} countryChoice={this.countryChoice}/>
                        <CountryInfo alpha3Code={this.state.selectedCountryCode}/>
                    </Row>
                </Container>
            </Fragment>
        );
    }
}

export default Home;