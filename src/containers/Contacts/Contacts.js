import React, {Component} from 'react';
import NavMenu from "../../components/NavMenu/NavMenu";
import {Route} from "react-router-dom";
import Magic from "../../components/Magic/Magic";
import {NavLink} from "reactstrap";

class Contacts extends Component {
    state = {
        magic: {name: 'Vunsh-', surname: "Punsh"}
    };
    checkoutContinuedHandler = () => {
        this.props.history.replace('/contacts/contact-data');
    };

    render() {
        return (
            <div>
                <NavMenu>
                    <NavLink href="" onClick={this.checkoutContinuedHandler}>Bam</NavLink>
                </NavMenu>
                <h1>Какой-то контент еще</h1>
                <Route path={this.props.match.path + '/contact-data'}
                       render={(props) => (<Magic mag={this.state.magic}{...props}/>)}
                />
            </div>
        );
    }
}

export default Contacts;