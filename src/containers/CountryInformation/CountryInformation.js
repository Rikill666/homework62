import React, {Component} from 'react';
import CountryInfo from "../../components/CountryInfo/CountryInfo";
import NavMenu from "../../components/NavMenu/NavMenu";

class CountryInformation extends Component {
    state = {
        selectedCountryCode: ""
    };

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search);
        let selectedCountryCode = "";
        for (let param of query.entries()) {
             selectedCountryCode = param[1];
        }
        this.setState({selectedCountryCode});
    }

    render() {
        return (
            <div>
                <NavMenu/>
                <CountryInfo alpha3Code={this.state.selectedCountryCode}/>
            </div>
        );
    }
}

export default CountryInformation;