import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";
import Contacts from "./containers/Contacts/Contacts";
import AboutUs from "./containers/AboutUs/AboutUs";
import {Container} from "reactstrap";
import CountryInformation from "./containers/CountryInformation/CountryInformation";


class App extends Component {

    render() {
        return (
            <Container>
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        <Route path="/contacts" component={Contacts}/>
                        <Route path="/aboutus" component={AboutUs}/>
                        <Route path="/checkout" component={CountryInformation}/>
                        <Route render={() => <h1>Not Found</h1>}/>
                    </Switch>
                </BrowserRouter>
            </Container>
        )
    }
}

export default App;
